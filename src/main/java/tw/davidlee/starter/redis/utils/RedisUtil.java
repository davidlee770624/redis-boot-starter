package tw.davidlee.starter.redis.utils;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class RedisUtil {

    @Autowired
    RedissonClient redissonClient;
    /**
     *    多線呈處理
     */
    ThreadLocal<Map<String,RLock>> localLocks = new ThreadLocal<>();

    /**
     *    上鎖
     *    redisson會自動為鎖增加expire時間 , 並監測取得鎖的線程是否還在執行 ,
     *    還在執行則延遲超時時間 , 其他線程沒有取得鎖則循環嘗試加鎖
     */
    public void lock(String key){
        RLock rLock = redissonClient.getLock(key);
        Map<String,RLock> localLock = localLocks.get();
        if(Objects.isNull(localLock))
            localLock = new ConcurrentHashMap<>();
        localLock.put(key,rLock);
        localLocks.set(localLock);
        rLock.lock();
    }

    /**
     *    解鎖
     */
    public void unLock(String key){
        Map<String,RLock> localLock = localLocks.get();
        if(Objects.isNull(localLock))
            return;
        RLock rLock = localLock.get(key);
        rLock.unlock();
        localLock.remove(key);
    }



}
