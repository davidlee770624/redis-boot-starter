package tw.davidlee.starter.redis.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "tw.davidlee.redis" )
public class RedissonConfigProperties {
    /**
     * 測試YAML提示
     * */
    private String test;
}
