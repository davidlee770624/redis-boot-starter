package tw.davidlee.starter.redis.config;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.redisson.spring.starter.RedissonAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import tw.davidlee.starter.redis.config.properties.RedissonConfigProperties;

import java.io.IOException;

/**
 *
 */
@Slf4j
@Configuration
@EnableCaching
@EnableConfigurationProperties(RedissonConfigProperties.class)
@AutoConfigureBefore(RedissonAutoConfiguration.class)
@ComponentScan(value="tw.davidlee.starter.redis.utils")
public class RedissonConfig extends CachingConfigurerSupport{

    @Autowired
    private RedissonConfigProperties RedissonConfigProperties;

    /**
     * 創建 集群連線池
     * */
    @Bean(destroyMethod="shutdown")
    RedissonClient redisson() throws IOException {
        // 两种读取方式，Config.fromYAML 和 Config.fromJSON
        // 此為本地端 是否能改成spring config?
        Config config = Config.fromYAML(RedissonConfig.class.getClassLoader().getResource("redisson-config.yml"));
        return Redisson.create(config);
    }

    /**
     * 結合spring cache
     * */
    @Bean
    CacheManager cacheManager(RedissonClient redissonClient) {
        return new RedissonSpringCacheManager(redissonClient);
    }

    /**
     * 預設key產生方式
     * Spring同样提供了方案：继承CachingConfigurerSupport并重写keyGenerator()
     * */
    @Override
    public KeyGenerator keyGenerator() {
        return (target, method, params) -> {
            StringBuilder sb = new StringBuilder();
            sb.append("CacheKey:");
            sb.append(target.getClass().getSimpleName());
            sb.append('.').append(method.getName()).append(":");

            // 這裏需要注意，參數太多的話考慮使用其他拼接方式
            for (Object obj : params) {
                if (obj != null) {
                    sb.append(obj.toString());
                    sb.append(".");
                }
            }
            return sb.toString();
        };
    }

}